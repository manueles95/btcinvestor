import React, { Component } from 'react';
import Header from './components/Header';
import BChart from './components/BChart'
import ClientInfo from './components/ClientInfo'
import './App.css';




class App extends Component {
  constructor (props) {
    super(props)

    // chart.js defaults



    this.state = {

      nombre: null,
      apellidos: null,
      saldo_mxn:null,
      saldo_btc:null,
      usr_name: null,
      usr_pwd:null,
      ultimo_precio_comprado:null,
      porcentaje_inversion:null
    }

    //this.onCurrencySelect = this.onCurrencySelect.bind(this)
  }

  componentWillMount(){
    this.getClientInfo();
  }

  getClientInfo(){
    // var url = "http://localhost:3031/cliente/123432"
    var url = "http://localhost:3030/cliente/manueles95"
    var request = new XMLHttpRequest();
    request.open("GET", url);
    request.setRequestHeader("Accept", "application/json", "Content-Type": "application/json");
    request.send();

    console.log("Getting this as a response");
    console.log(request);
    console.log(request.response);
    console.log(JSON.stringify(request.response))
    // this.setState(JSON.parse(request.responseText))


    console.log(this.state);
  }//end getClientInfo



  render() {
    if (true) {
      return (
        <div className="app">
          <Header title="BBVA" />

          <div className="client-container">
            <h3 className="welcome">Bienvenido</h3>
            <h1 className="clientName">{this.state.nombre}             {this.state.apellidos} </h1>


          </div>

          <div className='divided-pane'>

          <BChart/>
          <ClientInfo saldo_btc={this.state.saldo_btc} saldo_mxn={this.state.saldo_mxn} porcentaje_inversion={this.state.porcentaje_inversion} />

          </div>



        </div>
      )
    }

    return null
  }
}

export default App;

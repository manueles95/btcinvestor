import React, {Component} from 'react'
import './ClientInfo.css'



class ClientInfo extends Component{

  constructor (props){
    super(props)

    this.state = {
      saldo_mxn: 0.0,
      saldo_btc: 0.0,
      currencyPrice: 0.0,
      ultimo_precio_comprado: 0.0,
      porcentaje_inversion: 0
    }

  }

  getBitcoinData(){
    // var url = "http://localhost:3031/getCurrentPrice/btc"
    // var request = new XMLHttpRequest();
    // request.open("GET", url);
    // request.setRequestHeader("Accept", "application/json");
    // request.send();



      var result = {
        currencyType:"btc",
        currencyPrice:220000.00

      };

    // console.log(JSON.parse(request.responseText));
    // this.setState(JSON.parse(request.responseText))
    this.setState(result)
  }//end getClientInfo

  getRow(text, data){
    return(
      <div className="row">
        <div className="col s6">
          <h5>{text}</h5>
        </div>
        <div className="col s6">
          <h4 className="number">{data}</h4>
        </div>
      </div>
      )
  }

  componentWillMount(){
    this.loadData();
  }

  loadData(){
    this.setState(this.props);
    this.getBitcoinData();
  }

  render () {
    return(

      <div className='clientInfo'>



      <div className="container">
        {this.getRow("Balance MXN : ", this.state.saldo_mxn)}
        {this.getRow("Balance Bitcoin: ", this.state.saldo_btc)}
        {this.getRow("Valor actual: ", this.state.currencyPrice)}
        {this.getRow("Ultimo valor de compra: ", this.state.ultimo_precio_comprado)}
        {this.getRow("Porcentaje:", this.state.porcentaje_inversion)}
        {console.log(this.state)}
        <div className="row">
          <div className="col s6">
          <button type="buttton" className="btn comprar">Comprar</button>
          </div>
          <div className="col s6">
            <button type="buttton" className="btn vender">Vender</button>
          </div>
        </div>
      </div>



    </div>
      )
  }

}

export default ClientInfo;

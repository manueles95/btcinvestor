import React, { Component } from 'react';
import './BChart.css'
import _ from 'lodash';
import { Line, Chart } from 'react-chartjs-2';
import moment from 'moment';




class BChart extends Component {
  constructor (props) {
    super(props)

    // chart.js defaults
    Chart.defaults.global.defaultFontColor = 'white';
    Chart.defaults.global.defaultFontSize = 20;


    this.state = {
      historicalData: null,
      currency: "MXN",
      baseUrl: 'https://api.coindesk.com/'
    }
    //this.onCurrencySelect = this.onCurrencySelect.bind(this)
  }

  componentDidMount () {
    this.getBitcoinData()
  }

  getBitcoinData () {

    const {baseUrl, currency} = this.state

    fetch(`${baseUrl}v1/bpi/historical/close.json?currency=${currency}`)
      .then(response => response.json())
      .then(historicalData => this.setState({historicalData}))
      .catch(e => e)
  }

  formatChartData () {
    const {bpi} = this.state.historicalData

    return {
      labels: _.map(_.keys(bpi), date => moment(date).format("ll")),
      datasets: [
        {
          label: "Bitcoin",
          fill: true,
          lineTension: 0.1,
          backgroundColor: 'rgba(94, 86, 184)',
          borderColor: 'white',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'rgba(94, 86, 184)',
          pointBackgroundColor: '#fff',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(94, 86, 184)',
          pointHoverBorderColor: 'white',
          pointHoverBorderWidth: 2,
          pointRadius: 5,
          pointHitRadius: 10,
          data: _.values(bpi)
        }
      ]
    }
  }


  render() {
    if (this.state.historicalData) {
      return (
        <div className="BChart">
              <Line data={this.formatChartData()} height={250} />
        </div>
      )
    }

    return null
  }
}

export default BChart;
